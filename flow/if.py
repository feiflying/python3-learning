'''
流程控制语句
    注意判断后边的冒号不要丢失；
    还有python 没有&&、 ||、 ！逻辑运算符，它采用 and、or、not体现
'''

height = 1.75
weight = 80.5
bmi = weight/(height * height)
print(bmi)

if bmi < 18.5:
    print('过轻')
elif (bmi >= 18.5 and bmi <= 25):
    print('正常')
elif (bmi >25 and bmi <= 28):
    print('过重')
else:
    print('肥胖');

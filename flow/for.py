'''
迭代list
注意：
    冒号不要丢失
    for后无括号
'''
names = ['ll', 'dd', 'ee', 'uu']
for name in names :
    if name == 'ee':
        print('this is ee')
        break
    print(name)

"""
break与continue的用法与其他语言都一样
"""

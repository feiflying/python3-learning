'''
python类的集合包括：
    list和tuple
        list是一种有序的、可修改的集合,用[]初始化
        tuple是一种有序的不能修改（指向位置不可修改）的集合，用()初始化

'''
classmates = ('sj', 'dz', 1, True)
print(classmates)

'''
注意：
如果要定义一个只有1个元素的tuple，需要在元素后边增加逗号
'''
t = (1,)
print(t)
'''
python类的集合包括：
    list和tuple
        list是一种有序的、可修改的集合
        tuple是一种有序的不能修改（指向位置不可修改）的集合
'''

classmates =['cf','dndn']
#输出
print(classmates)
# list长度
print('len',len(classmates))
# 索引取值
print('第一个值',classmates[0])
# 倒数索引取值
print('最后一个值', classmates[-1])
# 追加元素到末尾
classmates.append('lala')
print(classmates)
# 将元素插入指定位置(0是队首)
classmates.insert(1,'fa')
print(classmates)
# 删除末尾元素
classmates.pop()
print(classmates)
# 删除指定位置的元素(0是队首)
classmates.pop(1)
print(classmates)
# 替换内容
classmates[0] = 'first'
print(classmates)

# list内的元素可以类型不一致，这一点与js一致，符合动态语言的特性。
newList = ['a',123,True,['1',2]]
print(newList); 
